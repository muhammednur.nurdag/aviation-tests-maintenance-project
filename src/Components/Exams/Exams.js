import React, { Component } from "react";
import { Link } from "react-router-dom";
import { database, auth } from "../../Database/Firebase";
import home from "../../Styles/Home/home-icon-yellow.png";
import ExamsList from './ExamsList';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import InvalidAuth from "../Authentication/InvalidAuth"

class Exams extends Component {
    constructor() {
        super()
        this.state = {
            exams: [],
            isUserSignedIn: false
        }
    }

    showConfirmPopup = function (event, category, type, date, onRemoveExam, keyVal, examTakers) {
        event.preventDefault();
        confirmAlert({
            title: 'DELETE EXAM',
            message: 'Are you sure to delete:',
            customUI: ({ title, message, onClose }) =>
                <div className="w-80 px-10 py-8 bg-white font-family:Arial text-gray-600 text-center border rounded border-gray-600">
                    <p className="w-full mb-1">{title}</p>
                    <p className="w-full mb-3">
                        {message}<br />
                        {category}<br />
                        {type}<br />
                        {date}<br />
                    </p>
                    <div className="grid grid-cols-2 gap-x-2">
                        <button className="w-full rounded text-white bg-lime-400 hover:bg-lime-600 focus:outline-none"
                            onClick={(event) => { onRemoveExam(keyVal, this.showResultPopup, examTakers); onClose(); }}>
                            YES
                        </button>
                        <button className="w-full rounded text-white bg-red-400 hover:bg-red-600 focus:outline-none"
                            onClick={(event) => { onClose() }}>
                            NO
                        </button>
                    </div>

                </div>
        });
    };

    showResultPopup = function (popupMessage) {
        confirmAlert({
            title: 'DELETE EXAM',
            message: popupMessage,
            customUI: ({ title, message, onClose }) =>
                <div className="w-80 px-10 py-8 bg-white font-family:Arial text-gray-600 text-center border rounded border-gray-600">
                    <p className="w-full mb-1">{title}</p>
                    <p className="w-full mb-3">{message}</p>
                    <div className="">
                        <button className="w-full rounded text-white bg-lime-400 hover:bg-lime-600 focus:outline-none"
                            onClick={(event) => onClose()}>
                            OK
                        </button>
                    </div>

                </div>
        });
    };

    async fetchExams() {
        const examRef = database.ref('exam_dates/').orderByChild('date');
        await examRef.on("value", (snapshot) => {
            let exam = [];
            snapshot.forEach((childSnapshot) => {
                let exam_takers = [];
                childSnapshot.child('exam_takers').forEach((childSnapshot) => {
                    exam_takers.push({ 'key': childSnapshot.key, 'value': childSnapshot.val() })
                });
                exam.push({
                    'key': childSnapshot.key,
                    'value': childSnapshot.val(),
                    'exam_takers': exam_takers
                });
            })
            this.setState({ exams: exam })
        })
    }

    componentDidMount() {
        this.fetchExams();
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        return (
            <div className="h-screen break-words px-20 pb-8">
                <div className="h-1/5 flex font-sans text-3xl font-semibold tracking-wider text-left pt-8 text-yellow-400 border-b-2 border-yellow-200">
                    <h2 className="w-1/2 pt-4">List Exams</h2>
                    <Link className="w-16 ml-auto" to="/home">
                        <img className="rounded-full" src={home} alt="yellow" />
                    </Link>
                </div>
                <div className="grid grid-cols-3 gap-x-8 gap-y-0">
                    {this.state.exams.map((exam, index) =>
                        <ExamsList
                            key={exam.key}
                            exam={exam}
                            index={index}
                            showConfirmPopup={this.showConfirmPopup}
                            showResultPopup={this.showResultPopup}
                        />)}
                </div>
            </div>
        )
    }

}

export default Exams