import React, { Component } from "react";
import { Link } from "react-router-dom";
import Moment from 'react-moment';
import { database, auth } from "../../Database/Firebase";
import InvalidAuth from "../Authentication/InvalidAuth"

class ExamsList extends Component {

    constructor() {
        super();
        this.state = {
            isUserSignedIn: false
        }
    }

    async onRemoveExam(k, showResultPopup, examTakers) {

        examTakers.forEach((taker) => {
            database.ref(`/students/${taker.key}/taken_exams/${k}`).remove().then((error) => {
                if (error)
                    console.log(error);
            });
        });

        let errorObject = false;
        await database.ref(`/exam_dates/${k}`).remove().then((error) => {
            if (error) {
                errorObject = error;
            }
        });
        if (errorObject)
            showResultPopup("Couldn't remove exam!");
        else
            showResultPopup("Exam removed successfully.");
    }

    componentDidMount() {
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        const value = this.props.exam.value;
        return (
            <div className="mt-5 h-48 w-full rounded-2xl border-2 border-yellow-200 shadow-lg">
                <div className="block py-4 h-36 w-full rounded-t-xl bg-yellow-300 text-white text-left text-xl">
                    <div className="px-10">
                        <label className="text-bold text-blue-700">Date: <Moment format="DD-MMMM-YYYY">{value.date}</Moment></label>
                    </div>
                    <div className="px-10 uppercase">
                        <label className="text-bold text-blue-700">Exam: {value.category.replace(/_/g, " ")}</label>
                    </div>
                    <div className="px-10">
                        <label className="text-bold text-blue-700">Type: {value.type.substring(0, 50)}</label>
                    </div>
                </div>
                <div className="block h-12 rounded-b-2xl grid grid-cols-2 gap-x-0 text-center">
                    <Link
                        to={{
                            pathname: `/exam/${this.props.exam.key}`,
                            state: {
                                examProps: this.props.exam
                            }
                        }}>
                        <div className="h-11 w-full rounded-bl-xl border-r-2 hover:bg-gray-200 bg-contain bg-center bg-no-repeat bg-assign"></div>
                    </Link>
                    <button className="h-11 w-full rounded-br-xl hover:bg-gray-200 bg-contain bg-center bg-no-repeat bg-delete"
                        onClick={(event) => {
                            const keyVal = this.props.exam.key;
                            this.props.showConfirmPopup(event,
                                value.category.replace(/_/g, " "),
                                value.type.substring(0, 50),
                                value.date, this.onRemoveExam, keyVal,
                                this.props.exam.exam_takers)
                        }}
                    ></button>
                </div>

            </div >
        )
    }
}

export default ExamsList