import React, { Component } from "react";
import { Link } from "react-router-dom";
import { auth } from "../../Database/Firebase";
import InvalidAuth from "../Authentication/InvalidAuth"

class StudentsList extends Component {

    constructor() {
        super();
        this.state = {
            isUserSignedIn: false
        }
    }

    componentDidMount() {
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        return (
            <div>
                <Link to={{
                    pathname: `/student/${this.props.student.key}`,
                    state: {
                        studentProps: this.props.student
                    }
                }}>
                    <div className="my-6 mx-auto h-12 py-2 block w-full rounded-2xl bg-lila hover:bg-purple-500 text-white outline-none text-center">
                        {this.props.student.value.name} {this.props.student.value.surname}
                    </div>
                </Link>
            </div>
        )
    }
}

export default StudentsList