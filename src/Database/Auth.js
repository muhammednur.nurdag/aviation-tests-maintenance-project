import { auth } from "../Database/Firebase";

export function login(event, email, password) {
    event.preventDefault();
    auth.signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            // Signed in
            var user = userCredential.user;
            if (user !== null) {
                console.log(this.props)
                this.props.history.push('/');
            }
        })
        .catch((error) => {
            console.log(error.code + error.message)
        });

};